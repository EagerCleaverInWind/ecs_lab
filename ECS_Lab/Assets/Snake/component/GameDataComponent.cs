using UnityEngine;

[DisallowMultipleComponent]
public class GameDataComponent : MonoBehaviour
{
    public Vector3 roomSize=new Vector3(24f,24f,0f);

    //singleton pattern in Uninty3D
    public static GameDataComponent instance=null;
    void Awake()
    {
        //for singleton
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy (gameObject);
    }

}

//DO NOT ADD MONOBEHAVIOUR MESSAGES (Start, Update, etc.)
