using UnityEngine;

[DisallowMultipleComponent]
public class PickUpComponent : MonoBehaviour
{
    public float points=10;
    public float size=1f;
    public enum PickUpAnimationType{STATIC,ROTATE,CHANGE_SIZE,TWINKLE};
    PickUpAnimationType animationType=PickUpAnimationType.STATIC;
    //音效，system据其给Audio Source component map AudioClip
    //string AudioClipStr;
    public float rebornDeltaTime=2f;
}
    
