﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public struct SnakeBodyNode//默认private、无法与一些库函数适配、List坑…C#的struct是真的鸡肋
public class SnakeBodyNode//只能每个元素都要new了。。。
{
    public Transform trans;
    //public enum NodeDirection{UP,DOWN,LEFT,RIGHT};
    //public NodeDirection direction;
    public Vector3 direction;//=Vector3.right;
}

[DisallowMultipleComponent]
public class SnakeComponent : MonoBehaviour 
{
    public GameObject nodePrefab;
    public List<SnakeBodyNode> nodes=new List<SnakeBodyNode>();
    public Vector3 nextDirection=Vector3.right;
    public Vector3 initPos=Vector3.zero;//i.e. head position——》不需要，就用本transform的位置吧——》需要！本trans固定为0子L才好是W
    public int initNodeNum=1;
    public float nodeDistance=0f;
    public float nodeSize=1f;
    public float moveStep;//nodeDistance+nodeSize
    public float speed=0f;
    public float moveAccumulator=0f;
}
