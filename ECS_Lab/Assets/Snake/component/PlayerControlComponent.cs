using UnityEngine;

[DisallowMultipleComponent]
public class PlayerControlComponent : MonoBehaviour
{
    public uint playerId;

    //direction
    public KeyCode up=KeyCode.W;
    public KeyCode down=KeyCode.S;
    public KeyCode left=KeyCode.A;
    public KeyCode right=KeyCode.D;
}

//DO NOT ADD MONOBEHAVIOUR MESSAGES (Start, Update, etc.)
