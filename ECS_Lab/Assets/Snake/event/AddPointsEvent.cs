using UnityEngine;

public class AddPointsEvent : EgoEvent
{
    public readonly uint playerId;
    public readonly float pointsAdd;

    public AddPointsEvent(uint playerId,float pointsAdd)
	{
        this.playerId = playerId;
        this.pointsAdd = pointsAdd;
	}
}
