using UnityEngine;

public class PickUpEvent : EgoEvent
{
    public readonly EgoComponent picker;
    public readonly EgoComponent pickee;//哈哈，并没有这个单词…

    public PickUpEvent(EgoComponent picker,EgoComponent pickee)
	{
        this.picker = picker;
        this.pickee = pickee;
	}
}
