using UnityEngine;
using UnityEngine.EventSystems;

public enum UnityEventType
{
    POINTER_ENTER,
    POINTER_EXIT,
    POINTER_DOWN,
    POINTER_UP,
    POINTER_CLICK,
    //待补
};

public class UnityEvent : EgoEvent
{
    public readonly UnityEventType type;
    public readonly BaseEventData baseData;//根据type到时自己向下转换。因为传的是引用（废话！C#啊），所以不必担心分割问题
    public readonly EgoComponent egoComponent;//事件发生对象,其实BaseEventData.selectedObject已经存了
    
    public UnityEvent(UnityEventType type,BaseEventData baseData,EgoComponent egoComponent)
	{
        this.type = type;
        this.baseData = baseData;
        this.egoComponent = egoComponent;
	}
}
