using UnityEngine;

public class SnakeCollisionEvent : EgoEvent
{
    //public readonly EgoComponent snake;
    public readonly SnakeComponent snake;
    public readonly EgoComponent other;

    public SnakeCollisionEvent(SnakeComponent snake,EgoComponent other)
	{
        this.snake = snake;
        this.other = other;
	}
}
