﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//不再attach，具体collider消息统一到中转站分发了
[RequireComponent( typeof( EgoComponent ) ) ]
public class WallColliderAdapter : MonoBehaviour {

    EgoComponent egoComponent;

    void Awake()
    {
        egoComponent = GetComponent<EgoComponent>();
    }

    //void OnTriggerEnter2D( Collider2D collider2d )
    void OnCollisionEnter2D( Collision2D collision )
    {
        //是不是蛇
        //SnakeComponent snake= collision.gameObject.GetComponent<SnakeComponent>();
        SnakeComponent snake= collision.gameObject.GetComponentInParent<SnakeComponent>();
        if (snake != null)
        {
            var e = new SnakeCollisionEvent(snake, egoComponent);
            EgoEvents<SnakeCollisionEvent>.AddEvent( e );
        }

    }
}
