﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeGameController : MonoBehaviour {
    //public GameObject snakeNodePrefab;

    static SnakeGameController()//这是private static构造函数？！新的singleton实现方式？！待解
    {
        EgoSystems.Add(
            new PlayerControlSnakeSystem(),
            new SnakeSystem(),
            new MessageDispatcherSystem(),
            new PickUpSystem(),
            new PlayerDataSystem(),
            new GameOverUISystem()
        );
    }

    void Start() 
    {
        /*EgoSystems.Add(
            new PlayerControlSnakeSystem(),
            new SnakeSystem(snakeNodePrefab)
        );*/
        EgoSystems.Start();
    }

    void Update() 
    {
        EgoSystems.Update();
    }

    void FixedUpdate() 
    {
        EgoSystems.FixedUpdate();
    }
}
