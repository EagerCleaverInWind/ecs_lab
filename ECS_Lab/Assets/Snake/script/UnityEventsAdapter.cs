﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent( typeof( EgoComponent ) ) ]
public class UnityEventsAdapter : MonoBehaviour {
    EgoComponent egoComponent;

    void Awake()
    {
        egoComponent = GetComponent<EgoComponent>();
    }

    public void OnPointerClick(BaseEventData data)
    {
        EgoEvents<UnityEvent>.AddEvent(new UnityEvent(UnityEventType.POINTER_CLICK, data, egoComponent));
    }

    //其他unity events待补
}
