using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;//暂时直接在这处理

public class MessageDispatcherSystem : EgoSystem
{
	public override void Start()
	{
        EgoEvents<CollisionEnter2DEvent>.AddHandler(Handle);
        EgoEvents<TriggerEnter2DEvent>.AddHandler(Handle);
        EgoEvents<UnityEvent>.AddHandler(Handle);
	}

    void Handle(CollisionEnter2DEvent e)
    {
        //避免二次重复，只管一边
        
        //whether snake
        if (e.egoComponent2.HasComponents<SnakeNodeComponent>())
        {
            EgoEvents<SnakeCollisionEvent>.AddEvent( new SnakeCollisionEvent(e.egoComponent2.GetComponentInParent<SnakeComponent>(),
                e.egoComponent1));
        }
            
    }

    void Handle(TriggerEnter2DEvent e)
    {
        if (e.egoComponent2.HasComponents<SnakeNodeComponent>())
        {
            if (e.egoComponent1.HasComponents<PickUpComponent>())
            {
                //EgoComponent snakeEgoInit = e.egoComponent2.GetComponentInParent<EgoComponent>();//debug发现得到的竟然是nodePrefab？！
                //SnakeComponent snake=e.egoComponent2.GetComponentInParent<SnakeComponent>();
                //snake.speed = 0f;//debug发现却有用！
                //EgoComponent snakeEgoDebug=snake.GetComponent<EgoComponent>();//debug发现得到的确实是snake
                //看来找parent EgoComponent不能用unity自带的GetComponentInParent<EgoComponent>()，Ego hierarchy有bug

                EgoComponent snakeEgo = e.egoComponent2.parent;
                EgoEvents<PickUpEvent>.AddEvent(new PickUpEvent(snakeEgo, e.egoComponent1));
            }
        }
    }
	
    void Handle(UnityEvent e)
    {
        switch (e.type)
        {
            case UnityEventType.POINTER_CLICK:
                PointerEventData pointerData = (PointerEventData)e.baseData;
                if (e.egoComponent.HasComponents<RestartButtonComponent>())
                {
                    //本该进一步发送具体消息，暂时直接在这处理
                    //Application.LoadLevel(Application.loadedLevel);
                    //直接替换掉原scene会出错，因为还有残留在运行。看来还是要将system魔改成MonoBehaviour啊
                    //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex,LoadSceneMode.Single);
                }
                break;

            default:
                break;
                
        }
    }

}