using UnityEngine;

public class PlayerControlSnakeSystem : EgoSystem
<
EgoConstraint<SnakeComponent,PlayerControlComponent>
>
{
	public override void Start()
	{
		
	}

	public override void Update()
	{
        constraint.ForEachGameObject( (ego, snake,playerControl) => {
            Vector3 inputDirection;
            if(Input.GetKeyDown(playerControl.up))
            {
                inputDirection=new Vector3(0,1,0);
            }
            else if(Input.GetKeyDown(playerControl.down))
            {
                inputDirection=new Vector3(0,-1,0);
            }
            else if(Input.GetKeyDown(playerControl.left))
            {
                inputDirection=new Vector3(-1,0,0);
            }
            else if(Input.GetKeyDown(playerControl.right))
            {
                inputDirection=new Vector3(1,0,0);
            }
            else//没按
            {
                return;
            }
            if(inputDirection==-snake.nodes[0].direction)//只允许转弯，不允许180度反转
                return;
            snake.nextDirection=inputDirection;
        });
	}

	public override void FixedUpdate()
	{
		
	}
}