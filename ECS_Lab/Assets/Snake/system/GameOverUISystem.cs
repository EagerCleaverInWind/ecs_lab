using UnityEngine;

public class GameOverUISystem : EgoSystem
<
EgoConstraint<GameOverUIComponent>
>
{
	public override void Start()
	{
        constraint.ForEachGameObject((ego, gameOverUI) =>
            {
                ego.gameObject.SetActive(false);
            });

        EgoEvents<GameOverEvent>.AddHandler(Handle);

	}
        
    void Handle(GameOverEvent e)
    {
        constraint.ForEachGameObject((ego, gameOverUI) =>
            {
                ego.gameObject.SetActive(true);
            });
    }

}