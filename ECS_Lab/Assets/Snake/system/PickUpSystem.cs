using UnityEngine;

public class PickUpSystem : EgoSystem
<
EgoConstraint<PickUpComponent>
>
{
	public override void Start()
	{
        EgoEvents<PickUpEvent>.AddHandler(Handle);
	}

    void Handle(PickUpEvent e)
    {
        e.pickee.gameObject.SetActive(false);
        //StartCoroutine(ReBorn(e.pickee));//唉，StartCoroutine属于MonoBehaviour
        //特么延时调用Invoke也属于属于MonoBehaviour！
        //唉，到时自己修改源码让EgoSystem也继承自MonoBehaviour，或自己写一个延时用CS
        e.pickee.transform.position = GetRebornPosition();
        e.pickee.gameObject.SetActive(true);
    }

    /*IEnumerator ReBorn(PickUpComponent pickUp)
    {
        yield return WaitForSeconds(pickUp.rebornDeltaTime);

        pickUp.transform.position = new Vector3(0, 0, 0);
        pickUp.gameObject.SetActive(true);
        yield return null;
    }*/
    Vector3 GetRebornPosition()
    {
        //random后还要检测是否出生在蛇身上。为了用到MonoBehaviour，想到的一个方法是先born一个试碰用的GO，再发回event
        //考虑到效率和复杂度问题，还是自己直接遍历node检测吧…
        float rangeX=GameDataComponent.instance.roomSize.x/2f,rangeY=GameDataComponent.instance.roomSize.y/2f;
        Vector3 pos=new Vector3(Random.Range(-rangeX,rangeX),Random.Range(-rangeY,rangeY),0);
        while (PositionAtSnake(pos))
        {
            pos=new Vector3(Random.Range(-rangeX,rangeX),Random.Range(-rangeY,rangeY),0);
        }
        return pos;
    }

    bool PositionAtSnake(Vector3 pos)
    {
        //不直接提供对含有某个component的所有GO collection的访问？！还要我用两次event获取？！唉……需要自己魔改源码了
        return false;
    }
}