using UnityEngine;
using UnityEngine.UI;

public class PlayerDataSystem : EgoSystem
<
EgoConstraint<PlayerDataComponent,Text>
>
{
	public override void Start()
	{
        constraint.ForEachGameObject((ego, playerData,textComponent) =>
            {
                playerData.points=0f;
                textComponent.text="player"+playerData.playerId
                    +"\npoints:"+playerData.points;
            });

        EgoEvents<AddPointsEvent>.AddHandler(Handle);
	}
        
    void Handle(AddPointsEvent e)
    {
        //这样foreach效率低，EgoCS要是直接提供筛选结果就好了…
        constraint.ForEachGameObject((ego, playerData,textComponent) =>
            {
                if(playerData.playerId!=e.playerId)
                    return;

                playerData.points+=e.pointsAdd;
                textComponent.text="player"+playerData.playerId
                    +"\npoints:"+playerData.points;
            });
    }

}