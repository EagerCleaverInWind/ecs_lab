﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnakeSystem : EgoSystem
<
    EgoConstraint<SnakeComponent>
>
{
    //GameObject nodePrefab;——》放到component中
    /*public SnakeSystem(GameObject nodePrefab)
    {
        this.nodePrefab = nodePrefab;
    }*/

    public override void Start()
    {
        constraint.ForEachGameObject( (ego, snakeComponent) => {
            //set move step——》size可能与prefab实际size（collider & sprite）不一致，待统一
            snakeComponent.moveStep=snakeComponent.nodeSize+snakeComponent.nodeDistance;
            //add head
            AddBodyNode(snakeComponent,true);
            //add intial body nodes
            for(int i=0;i<snakeComponent.initNodeNum-1;i++)
            {
                AddBodyNode(snakeComponent);
            }
        });

        //register event handler
        EgoEvents<SnakeCollisionEvent>.AddHandler(Handle);
        EgoEvents<PickUpEvent>.AddHandler(Handle);
    }

    void Handle(SnakeCollisionEvent e)
    {
        //看具体是哪一条蛇
        //SnakeComponent snake = e.snake.gameObject.GetComponent<SnakeComponent>();

        //其实应该放到中转站细分到C-C层次，再来处理
        if (e.other.HasComponents<WallComponent>())
        {
            //debug
            //e.snake.enabled=false;//哈哈，现在逻辑根本不在这里，在想什么呢！待attach stop component
            Debug.Log("撞墙了！");
            //e.snake.gameObject.AddComponent<StopComponent>();
            Ego.AddComponent<StopComponent>(e.snake.GetComponent<EgoComponent>());
        }
        if (e.other.HasComponents<SnakeNodeComponent>())//蛇撞蛇
        {
            SnakeComponent otherSnake = e.other.GetComponentInParent<SnakeComponent>();
            if (e.snake == otherSnake)//自己撞到自己
            {
                Debug.Log("自己撞自己了！");
                //e.snake.gameObject.AddComponent<StopComponent>();
                Ego.AddComponent<StopComponent>(e.snake.GetComponent<EgoComponent>());
            }
            else
            {
                
            }
        }
        //其实应该放到细分层次，暂且作为统一操作放到这里吧
        EgoEvents<GameOverEvent>.AddEvent(new GameOverEvent());
    }

    void Handle(PickUpEvent e)
    {
        SnakeComponent snake = e.picker.gameObject.GetComponent<SnakeComponent>();
        //不需要，分发具体碰撞消息前已经检查过了
        /*if (snake != null)
        {
            if (e.pickee.HasComponents<PickUpComponent>())
            {
                
            }
        }*/
        AddBodyNode(snake);
        if (e.picker.HasComponents<PlayerControlComponent>())//玩家控制的需要计分显示
        {
            uint playerId = e.picker.GetComponent<PlayerControlComponent>().playerId;
            PickUpComponent pickUp = e.pickee.GetComponent<PickUpComponent>();
            EgoEvents<AddPointsEvent>.AddEvent(new AddPointsEvent(playerId,pickUp.points));
        }

    }

    public override void FixedUpdate ()
    {
        constraint.ForEachGameObject( (ego, snakeComponent) => {
            if(ego.HasComponents<StopComponent>())
            {
                //本来想在这减时间，才一两句代码。但为了维护和拓展，还是设一个专门的system吧
                return;
            }    
            snakeComponent.moveAccumulator+=snakeComponent.speed*Time.deltaTime;
            if(snakeComponent.moveAccumulator>snakeComponent.moveStep)
            {
                MoveSnake(snakeComponent);
                snakeComponent.moveAccumulator-=snakeComponent.moveStep;
            }
        });
    }

    //one step
    void MoveSnake(SnakeComponent snakeComponent)
    {
        List<SnakeBodyNode> nodes = snakeComponent.nodes;
        for (int i = nodes.Count - 1; i >0; i--)
        {
            nodes[i].trans.position = nodes[i - 1].trans.position;
            nodes[i].direction = nodes[i - 1].direction;
        }
        //move head
        nodes[0].direction=snakeComponent.nextDirection;
        nodes[0].trans.position+=nodes[0].direction*snakeComponent.moveStep;
    }
        
    void AddBodyNode(SnakeComponent snakeComponent,bool isHead=false)
    {
        Vector3 position, direction;
        if (isHead)
        {
            position = snakeComponent.initPos;
            direction = snakeComponent.nextDirection;
        }
        else
        {
            SnakeBodyNode preTail = snakeComponent.nodes[snakeComponent.nodes.Count - 1];
            position = preTail.trans.position - preTail.direction * snakeComponent.moveStep;
            direction = preTail.direction;
        }

        SnakeBodyNode newNode = new SnakeBodyNode();
        newNode.direction = direction;
        //注意子GO node并不参与ECS系统，所以不需要使用Ego.xxx来创建——》哈哈改为碰撞统一适配后，就需要了
        //head.trans=GameObject.Instantiate(snakeComponent.nodePrefab,snakeComponent.initPos,Quaternion.identity).transform;
        newNode.trans=Ego.AddGameObject(GameObject.Instantiate(snakeComponent.nodePrefab,position,Quaternion.identity)).transform;
        newNode.trans.parent=snakeComponent.transform;
        //改用EgoCS的SetParent——》有bug，没卵用
        //Ego.SetParent(snakeComponent.GetComponent<EgoComponent>(),newNode.trans.GetComponent<EgoComponent>());
        snakeComponent.nodes.Add(newNode);
    }

}

